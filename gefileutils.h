//
// Created by Dan on 02.08.2022.
//

#ifndef LIBRARY_GLIB_EXT_GEFILEUTILS_H
#define LIBRARY_GLIB_EXT_GEFILEUTILS_H

#include "gemain.h"
#include "geerrno.h"










G_BEGIN_DECLS

gint ge_mkdir_with_parents(gchar *pathname, gint mode, GError **error);

G_END_DECLS










G_BEGIN_DECLS

gboolean ge_file_set_contents_mkdir(gchar *filename, gchar *contents, gssize length, GError **error);

G_END_DECLS










#endif //LIBRARY_GLIB_EXT_GEFILEUTILS_H
