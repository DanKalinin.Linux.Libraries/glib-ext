//
// Created by Dan on 02.08.2022.
//

#include "geschemas.h"

gboolean ge_schemas_set(gchar *file, GError **error) {
    if (g_file_test(file, G_FILE_TEST_EXISTS)) return TRUE;
    g_autofree gchar *directory = g_path_get_dirname(file);
    if (ge_mkdir_with_parents(directory, 0700, error) == -1) return FALSE;
    if (!g_file_set_contents(file, ge_schemas_data, ge_schemas_n, error)) return FALSE;
    return TRUE;
}
