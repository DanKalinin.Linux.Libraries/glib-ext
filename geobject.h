//
// Created by dan on 11.04.19.
//

#ifndef LIBRARY_GLIB_EXT_GEOBJECT_H
#define LIBRARY_GLIB_EXT_GEOBJECT_H

#include "gemain.h"
#include "gemisc.h"
#include "getype.h"










G_BEGIN_DECLS

#define GE_OBJECT_SET_PROPERTY_NAMED(object, property, Object, Property) \
    static inline void object ## _set_ ## property(Object *self, Property property) { \
        g_object_set(self, #property, property, NULL); \
    }

#define GE_OBJECT_GET_PROPERTY_NAMED(object, property, Object, Property) \
    static inline Property object ## _get_ ## property(Object *self) { \
        Property ret; \
        g_object_get(self, #property, &ret, NULL); \
        return ret; \
    }

#define GE_OBJECT_PROPERTY_NAMED(object, property, Object, Property) \
    GE_OBJECT_SET_PROPERTY_NAMED(object, property, Object, Property) \
    GE_OBJECT_GET_PROPERTY_NAMED(object, property, Object, Property)

#define GE_OBJECT_SET_PROPERTY_DATA(object, property, Object, Property, unref) \
    static inline void object ## _set_ ## property(Object *self, Property *property) { \
        g_object_set_data_full(G_OBJECT(self), #property, property, (GDestroyNotify)unref); \
    }

#define GE_OBJECT_GET_PROPERTY_DATA(object, property, Object, Property) \
    static inline Property *object ## _get_ ## property(Object *self) { \
        Property *ret = g_object_get_data(G_OBJECT(self), #property); \
        return ret; \
    }

#define GE_OBJECT_PROPERTY_DATA(object, property, Object, Property, unref) \
    GE_OBJECT_SET_PROPERTY_DATA(object, property, Object, Property, unref) \
    GE_OBJECT_GET_PROPERTY_DATA(object, property, Object, Property)

G_END_DECLS










G_BEGIN_DECLS

#define GE_TYPE_OBJECT ge_object_get_type()

GE_DECLARE_DERIVABLE_TYPE(GEObject, ge_object, GE, OBJECT, GObject)

struct _GEObjectClass {
    GObjectClass super;
};

struct _GEObject {
    GObject super;
};

G_END_DECLS










#endif //LIBRARY_GLIB_EXT_GEOBJECT_H
