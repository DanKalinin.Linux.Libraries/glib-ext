//
// Created by dan on 16.03.2022.
//

#ifndef LIBRARY_GLIB_EXT_GESTDIO_H
#define LIBRARY_GLIB_EXT_GESTDIO_H

#include "gemain.h"
#include "geerrno.h"

G_BEGIN_DECLS

gint ge_open(gchar *filename, gint flags, gint mode, GError **error);
gssize ge_read(gint self, gpointer buf, gsize count, GError **error);
gssize ge_write(gint self, gpointer buf, gsize count, GError **error);
gint ge_stat(gchar *filename, GStatBuf *buf, GError **error);
gint ge_unlink(gchar *filename, GError **error);
gint ge_rename(gchar *oldfilename, gchar *newfilename, GError **error);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GESTDIO_H
