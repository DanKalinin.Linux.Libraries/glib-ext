//
// Created by Dan on 08.03.2025.
//

#include "geglcmessages.h"

gboolean ge_g_lc_messages_set(gchar *file, GError **error) {
    gchar *language = (gchar *)g_getenv("LANG");
    if (g_str_equal(language, "ru")) return ge_file_set_contents_mkdir(file, ge_g_lc_messages_ru_data, ge_g_lc_messages_ru_n, error);
    return TRUE;
}
