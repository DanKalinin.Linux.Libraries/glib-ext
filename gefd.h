//
// Created by dan on 15.03.2022.
//

#ifndef LIBRARY_GLIB_EXT_GEFD_H
#define LIBRARY_GLIB_EXT_GEFD_H

#include "gemain.h"
#include "geobject.h"

G_BEGIN_DECLS

#define GE_TYPE_FD ge_fd_get_type()

GE_DECLARE_DERIVABLE_TYPE(GEFD, ge_fd, GE, FD, GEObject)

struct _GEFDClass {
    GEObjectClass super;
};

struct _GEFD {
    GEObject super;

    gint object;
};

GE_STRUCTURE_FIELD_INT(ge_fd, object, GEFD, gint, NULL, close, NULL)

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GEFD_H
