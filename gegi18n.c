//
// Created by Dan on 08.03.2025.
//

#include "gegi18n.h"

G_DEFINE_CONSTRUCTOR(ge_g_i18n_constructor)

static void ge_g_i18n_constructor(void) {
    (void)bindtextdomain(GETTEXT_PACKAGE, NULL);
    (void)bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}
