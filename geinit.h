//
// Created by root on 19.10.2021.
//

#ifndef LIBRARY_GLIB_EXT_GEINIT_H
#define LIBRARY_GLIB_EXT_GEINIT_H

#include "gemain.h"
#include "gemainthread.h"
#include "geschemas.h"
#include "geginit.h"

G_BEGIN_DECLS

void ge_init(void);
void ge_init_l10n(gchar *language);
void ge_init_i18n(gchar *directory);
void ge_init_po(void);
void ge_init_tz(gint seconds);
void ge_init_schemas(gchar *directory);
void ge_init_cwd(gchar *directory);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GEINIT_H
