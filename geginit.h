//
// Created by Dan on 08.03.2025.
//

#ifndef LIBRARY_GLIB_EXT_GEGINIT_H
#define LIBRARY_GLIB_EXT_GEGINIT_H

#include "gemain.h"
#include "geglcmessages.h"

G_BEGIN_DECLS

void ge_g_init(void);
void ge_g_init_i18n(gchar *directory);
void ge_g_init_po(void);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GEGINIT_H
