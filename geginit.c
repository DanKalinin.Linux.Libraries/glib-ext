//
// Created by Dan on 08.03.2025.
//

#include "geginit.h"
#include "gegi18n.h"

void ge_g_init(void) {

}

void ge_g_init_i18n(gchar *directory) {
    (void)bindtextdomain(GETTEXT_PACKAGE, directory);
    (void)bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}

void ge_g_init_po(void) {
    gchar *directory = bindtextdomain(GETTEXT_PACKAGE, NULL);
    gchar *language = (gchar *)g_getenv("LANG");
    g_autofree gchar *file = g_build_filename(directory, language, "LC_MESSAGES", GETTEXT_PACKAGE ".mo", NULL);
    if (!ge_g_lc_messages_set(file, NULL)) return;
}
