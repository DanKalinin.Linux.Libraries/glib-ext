//
// Created by dan on 18.11.2019.
//

#ifndef LIBRARY_GLIB_EXT_GETHREAD_H
#define LIBRARY_GLIB_EXT_GETHREAD_H

#include "gemain.h"
#include "geobject.h"

G_BEGIN_DECLS

#define GE_TYPE_THREAD ge_thread_get_type()

GE_DECLARE_DERIVABLE_TYPE(GEThread, ge_thread, GE, THREAD, GEObject)

struct _GEThreadClass {
    GEObjectClass super;

    gpointer (*run)(GEThread *self);
};

struct _GEThread {
    GEObject super;

    GThread *object;
};

GE_STRUCTURE_FIELD(ge_thread, object, GEThread, GThread, g_thread_ref, g_thread_unref, NULL)

gpointer ge_thread_run_function(gpointer self);
gpointer ge_thread_run(GEThread *self);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GETHREAD_H
