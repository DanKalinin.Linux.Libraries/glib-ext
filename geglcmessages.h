//
// Created by Dan on 08.03.2025.
//

#ifndef LIBRARY_GLIB_EXT_GEGLCMESSAGES_H
#define LIBRARY_GLIB_EXT_GEGLCMESSAGES_H

#include "gemain.h"
#include "gefileutils.h"

G_BEGIN_DECLS

extern gchar ge_g_lc_messages_ru_data[];
extern gint ge_g_lc_messages_ru_n;

gboolean ge_g_lc_messages_set(gchar *file, GError **error);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GEGLCMESSAGES_H
