//
// Created by Dan on 10.05.2023.
//

#include "gesignal.h"

gint ge_kill(GPid pid, gint signal, GError **error) {
    gint ret = kill(pid, signal);

    if (ret == -1) {
        ge_errno_set_error(error, errno);
    }

    return ret;
}
