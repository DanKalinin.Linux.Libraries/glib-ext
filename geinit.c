//
// Created by root on 19.10.2021.
//

#include "geinit.h"

void ge_init(void) {
    ge_g_init();
    (void)ge_main_thread_new();
}

void ge_init_l10n(gchar *language) {
    if (!g_setenv("LANG", language, TRUE)) return;
    if (!g_setenv("LC_CTYPE", "UTF-8", TRUE)) return;
}

void ge_init_i18n(gchar *directory) {
    ge_g_init_i18n(directory);
}

void ge_init_po(void) {
    ge_g_init_po();
}

void ge_init_tz(gint seconds) {
    gint h = seconds / 3600;
    gint m = seconds / 60 % 60;
    gint s = seconds % 60;
    if (m < 0) m = -m;
    if (s < 0) s = -s;
    g_autofree gchar *value = g_strdup_printf("%02i:%02i:%02i", h, m, s);
    if (!g_setenv("TZ", value, TRUE)) return;
}

void ge_init_schemas(gchar *directory) {
    g_autofree gchar *file = g_build_filename(directory, "glib-2.0", "schemas", "gschemas.compiled", NULL);
    if (!ge_schemas_set(file, NULL)) return;
    if (!g_setenv("XDG_DATA_DIRS", directory, TRUE)) return;
}

void ge_init_cwd(gchar *directory) {
    if (g_chdir(directory) == -1) return;
}
