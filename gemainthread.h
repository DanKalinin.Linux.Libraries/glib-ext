//
// Created by dan on 18.11.2019.
//

#ifndef LIBRARY_GLIB_EXT_GEMAINTHREAD_H
#define LIBRARY_GLIB_EXT_GEMAINTHREAD_H

#include "gemain.h"
#include "gethread.h"

G_BEGIN_DECLS

#define GE_TYPE_MAIN_THREAD ge_main_thread_get_type()

GE_DECLARE_DERIVABLE_TYPE(GEMainThread, ge_main_thread, GE, MAIN_THREAD, GEThread)

struct _GEMainThreadClass {
    GEThreadClass super;
};

struct _GEMainThread {
    GEThread super;
};

GEMainThread *ge_main_thread_new(void);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GEMAINTHREAD_H
