//
// Created by root on 15.01.2020.
//

#ifndef LIBRARY_GLIB_EXT_GESTRFUNCS_H
#define LIBRARY_GLIB_EXT_GESTRFUNCS_H

#include "gemain.h"

G_BEGIN_DECLS

#define GE_DUP_VALUE(value) g_memdup(&value, sizeof(value))

gchar *ge_strreplace(gchar *self, gchar *from, gchar *to);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GESTRFUNCS_H
