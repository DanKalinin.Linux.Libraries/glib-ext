//
// Created by dan on 18.11.2019.
//

#include "gethread.h"

enum {
    GE_THREAD_SIGNAL_RUN = 1,
    _GE_THREAD_SIGNAL_COUNT
};

guint ge_thread_signals[_GE_THREAD_SIGNAL_COUNT] = {0};

void ge_thread_finalize(GObject *self);
gpointer _ge_thread_run(GEThread *self);

G_DEFINE_TYPE(GEThread, ge_thread, GE_TYPE_OBJECT)

static void ge_thread_class_init(GEThreadClass *class) {
    G_OBJECT_CLASS(class)->finalize = ge_thread_finalize;

    class->run = _ge_thread_run;

    ge_thread_signals[GE_THREAD_SIGNAL_RUN] = g_signal_new("run", GE_TYPE_THREAD, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(GEThreadClass, run), NULL, NULL, NULL, G_TYPE_POINTER, 0);
}

static void ge_thread_init(GEThread *self) {

}

void ge_thread_finalize(GObject *self) {
    ge_thread_set_object(GE_THREAD(self), NULL);

    G_OBJECT_CLASS(ge_thread_parent_class)->finalize(self);
}

gpointer ge_thread_run_function(gpointer self) {
    return ge_thread_run(self);
}

gpointer ge_thread_run(GEThread *self) {
    gpointer ret = NULL;
    g_signal_emit(self, ge_thread_signals[GE_THREAD_SIGNAL_RUN], 0, &ret);
    return ret;
}

gpointer _ge_thread_run(GEThread *self) {
    g_print("run\n");
    return NULL;
}
