//
// Created by dan on 24.06.19.
//

#ifndef LIBRARY_GLIB_EXT_GEERRNO_H
#define LIBRARY_GLIB_EXT_GEERRNO_H

#include "gemain.h"

G_BEGIN_DECLS

#define GE_ERRNO_ERROR ge_errno_error_quark()

GQuark ge_errno_error_quark(void);

void ge_errno_set_error(GError **self, gint code);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GEERRNO_H
