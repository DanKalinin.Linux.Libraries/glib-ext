//
// Created by Dan on 10.05.2023.
//

#ifndef LIBRARY_GLIB_EXT_GESIGNAL_H
#define LIBRARY_GLIB_EXT_GESIGNAL_H

#include "gemain.h"
#include "geerrno.h"

G_BEGIN_DECLS

gint ge_kill(GPid pid, gint signal, GError **error);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GESIGNAL_H
