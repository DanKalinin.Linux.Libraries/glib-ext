//
// Created by dan on 31.03.19.
//

#ifndef LIBRARY_GLIB_EXT_GEMAIN_H
#define LIBRARY_GLIB_EXT_GEMAIN_H

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <locale.h>
#include <termios.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <arpa/inet.h>

#endif //LIBRARY_GLIB_EXT_GEMAIN_H
