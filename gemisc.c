//
// Created by root on 12.09.2021.
//

#include "gemisc.h"

gboolean ge_set_pointer(gpointer *location, gpointer value, gpointer none, GERefFunc ref, GEUnrefFunc unref, GEEqualFunc equal) {
    if (GE_CALL_FUNCTION_V1(equal, (*location == value), *location, value)) return FALSE;
    if (*location != none) GE_CALL_FUNCTION(unref, *location);
    *location = (value == none) ? none : GE_CALL_FUNCTION_V1(ref, value, value);
    return TRUE;
}

gboolean ge_set_int(gint *location, gint value, gint none, GEIntRefFunc ref, GEIntUnrefFunc unref, GEIntEqualFunc equal) {
    if (GE_CALL_FUNCTION_V1(equal, (*location == value), *location, value)) return FALSE;
    if (*location != none) GE_CALL_FUNCTION(unref, *location);
    *location = (value == none) ? none : GE_CALL_FUNCTION_V1(ref, value, value);
    return TRUE;
}

gboolean ge_set_uint(guint *location, guint value, guint none, GEUIntRefFunc ref, GEUIntUnrefFunc unref, GEUIntEqualFunc equal) {
    if (GE_CALL_FUNCTION_V1(equal, (*location == value), *location, value)) return FALSE;
    if (*location != none) GE_CALL_FUNCTION(unref, *location);
    *location = (value == none) ? none : GE_CALL_FUNCTION_V1(ref, value, value);
    return TRUE;
}
