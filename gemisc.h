//
// Created by root on 12.09.2021.
//

#ifndef LIBRARY_GLIB_EXT_GEMISC_H
#define LIBRARY_GLIB_EXT_GEMISC_H

#include "gemain.h"

G_BEGIN_DECLS

#define GE_SET_VALUE(location, value) \
    ({ \
        if (location != NULL) { \
            *location = value; \
        } \
    })

#define GE_CALL_FUNCTION(function, ...) \
    ({ \
        if (function != NULL) { \
            function(__VA_ARGS__); \
        } \
    })

#define GE_CALL_FUNCTION_V1(function, ret, ...) \
    ({ \
        (function != NULL) ? function(__VA_ARGS__) : ret; \
    })

#define GE_STRUCTURE_SET_FIELD(structure, field, Structure, Field, ref, unref, equal) \
    static inline void structure ## _set_ ## field(Structure *self, Field *field) { \
        (void)ge_set_pointer((gpointer *)&self->field, field, NULL, (GERefFunc)ref, (GEUnrefFunc)unref, (GEEqualFunc)equal); \
    }

#define GE_STRUCTURE_GET_FIELD(structure, field, Structure, Field, ref) \
    static inline Field *structure ## _get_ ## field(Structure *self) { \
        Field *ret = NULL; \
        (void)ge_set_pointer((gpointer *)&ret, self->field, NULL, (GERefFunc)ref, NULL, NULL); \
        return ret; \
    }

#define GE_STRUCTURE_FIELD(structure, field, Structure, Field, ref, unref, equal) \
    GE_STRUCTURE_SET_FIELD(structure, field, Structure, Field, ref, unref, equal) \
    GE_STRUCTURE_GET_FIELD(structure, field, Structure, Field, ref)

#define GE_STRUCTURE_SET_FIELD_INT(structure, field, Structure, Field, ref, unref, equal) \
    static inline void structure ## _set_ ## field(Structure *self, Field field) { \
        (void)ge_set_int((gint *)&self->field, field, -1, (GEIntRefFunc)ref, (GEIntUnrefFunc)unref, (GEIntEqualFunc)equal); \
    }

#define GE_STRUCTURE_GET_FIELD_INT(structure, field, Structure, Field, ref) \
    static inline Field structure ## _get_ ## field(Structure *self) { \
        Field ret = -1; \
        (void)ge_set_int((gint *)&ret, self->field, -1, (GEIntRefFunc)ref, NULL, NULL); \
        return ret; \
    }

#define GE_STRUCTURE_FIELD_INT(structure, field, Structure, Field, ref, unref, equal) \
    GE_STRUCTURE_SET_FIELD_INT(structure, field, Structure, Field, ref, unref, equal) \
    GE_STRUCTURE_GET_FIELD_INT(structure, field, Structure, Field, ref)

static inline gint ge_steal_int(gint *self) {
    gint ret = *self;
    *self = -1;
    return ret;
}

#define GE_STRUCTURE_SET_FIELD_UINT(structure, field, Structure, Field, ref, unref, equal) \
    static inline void structure ## _set_ ## field(Structure *self, Field field) { \
        (void)ge_set_uint((guint *)&self->field, field, 0, (GEUIntRefFunc)ref, (GEUIntUnrefFunc)unref, (GEUIntEqualFunc)equal); \
    }

#define GE_STRUCTURE_GET_FIELD_UINT(structure, field, Structure, Field, ref) \
    static inline Field structure ## _get_ ## field(Structure *self) { \
        Field ret = 0; \
        (void)ge_set_uint((guint *)&ret, self->field, 0, (GEUIntRefFunc)ref, NULL, NULL); \
        return ret; \
    }

#define GE_STRUCTURE_FIELD_UINT(structure, field, Structure, Field, ref, unref, equal) \
    GE_STRUCTURE_SET_FIELD_UINT(structure, field, Structure, Field, ref, unref, equal) \
    GE_STRUCTURE_GET_FIELD_UINT(structure, field, Structure, Field, ref)

static inline guint ge_steal_uint(guint *self) {
    guint ret = *self;
    *self = 0;
    return ret;
}

typedef gpointer (*GERefFunc)(gpointer self);
typedef void (*GEUnrefFunc)(gpointer self);
typedef gboolean (*GEEqualFunc)(gpointer a, gpointer b);

gboolean ge_set_pointer(gpointer *location, gpointer value, gpointer none, GERefFunc ref, GEUnrefFunc unref, GEEqualFunc equal);

typedef gint (*GEIntRefFunc)(gint self);
typedef void (*GEIntUnrefFunc)(gint self);
typedef gboolean (*GEIntEqualFunc)(gint a, gint b);

gboolean ge_set_int(gint *location, gint value, gint none, GEIntRefFunc ref, GEIntUnrefFunc unref, GEIntEqualFunc equal);

typedef guint (*GEUIntRefFunc)(guint self);
typedef void (*GEUIntUnrefFunc)(guint self);
typedef gboolean (*GEUIntEqualFunc)(guint a, guint b);

gboolean ge_set_uint(guint *location, guint value, guint none, GEUIntRefFunc ref, GEUIntUnrefFunc unref, GEUIntEqualFunc equal);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(gchar, g_free)
G_DEFINE_AUTO_CLEANUP_FREE_FUNC(gint, close, -1)

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GEMISC_H
