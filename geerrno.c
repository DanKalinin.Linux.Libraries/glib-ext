//
// Created by dan on 24.06.19.
//

#include "geerrno.h"

G_DEFINE_QUARK(ge-errno-error-quark, ge_errno_error)

void ge_errno_set_error(GError **self, gint code) {
    gchar *message = (gchar *)g_strerror(code);
    g_set_error_literal(self, GE_ERRNO_ERROR, code, message);
}
