//
// Created by dan on 18.11.2019.
//

#include "gemainthread.h"

gpointer ge_main_thread_run(GEThread *self);

G_DEFINE_TYPE(GEMainThread, ge_main_thread, GE_TYPE_THREAD)

static void ge_main_thread_class_init(GEMainThreadClass *class) {
    GE_THREAD_CLASS(class)->run = ge_main_thread_run;
}

static void ge_main_thread_init(GEMainThread *self) {

}

gpointer ge_main_thread_run(GEThread *self) {
    g_autoptr(GMainLoop) loop = g_main_loop_new(NULL, TRUE);
    g_main_loop_run(loop);
    g_object_unref(self);
    return NULL;
}

GEMainThread *ge_main_thread_new(void) {
    GEMainThread *self = g_object_new(GE_TYPE_MAIN_THREAD, NULL);
    GE_THREAD(self)->object = g_thread_new(NULL, ge_thread_run_function, self);
    return self;
}
