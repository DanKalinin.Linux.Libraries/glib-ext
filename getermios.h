//
// Created by dan on 16.03.2022.
//

#ifndef LIBRARY_GLIB_EXT_GETERMIOS_H
#define LIBRARY_GLIB_EXT_GETERMIOS_H

#include "gemain.h"
#include "geerrno.h"

G_BEGIN_DECLS

gint ge_tcgetattr(gint self, struct termios *termios_p, GError **error);
gint ge_tcsetattr(gint self, gint optional_actions, struct termios *termios_p, GError **error);
gint ge_cfsetspeed(struct termios *self, speed_t speed, GError **error);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GETERMIOS_H
