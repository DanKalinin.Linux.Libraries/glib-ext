//
// Created by Dan on 08.03.2025.
//

#ifndef LIBRARY_GLIB_EXT_GEGI18N_H
#define LIBRARY_GLIB_EXT_GEGI18N_H

#include "gemain.h"
#include "geconstructor.h"

G_BEGIN_DECLS

#define GETTEXT_PACKAGE "glib20"
#include <glib/gi18n-lib.h>

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GEGI18N_H
