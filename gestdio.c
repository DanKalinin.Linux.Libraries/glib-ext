//
// Created by dan on 16.03.2022.
//

#include "gestdio.h"

gint ge_open(gchar *filename, gint flags, gint mode, GError **error) {
    gint self = g_open(filename, flags, mode);

    if (self == -1) {
        ge_errno_set_error(error, errno);
    }

    return self;
}

gssize ge_read(gint self, gpointer buf, gsize count, GError **error) {
    gssize ret = read(self, buf, count);

    if (ret == -1) {
        ge_errno_set_error(error, errno);
    }

    return ret;
}

gssize ge_write(gint self, gpointer buf, gsize count, GError **error) {
    gssize ret = write(self, buf, count);

    if (ret == -1) {
        ge_errno_set_error(error, errno);
    }

    return ret;
}

gint ge_stat(gchar *filename, GStatBuf *buf, GError **error) {
    gint ret = g_stat(filename, buf);

    if (ret == -1) {
        ge_errno_set_error(error, errno);
    }

    return ret;
}

gint ge_unlink(gchar *filename, GError **error) {
    gint ret = g_unlink(filename);

    if (ret == -1) {
        ge_errno_set_error(error, errno);
    }

    return ret;
}

gint ge_rename(gchar *oldfilename, gchar *newfilename, GError **error) {
    gint ret = g_rename(oldfilename, newfilename);
    if (ret == -1) ge_errno_set_error(error, errno);
    return ret;
}
