//
// Created by Dan on 02.08.2022.
//

#ifndef LIBRARY_GLIB_EXT_GESCHEMAS_H
#define LIBRARY_GLIB_EXT_GESCHEMAS_H

#include "gemain.h"
#include "gefileutils.h"

G_BEGIN_DECLS

extern gchar ge_schemas_data[];
extern gint ge_schemas_n;

gboolean ge_schemas_set(gchar *file, GError **error);

G_END_DECLS

#endif //LIBRARY_GLIB_EXT_GESCHEMAS_H
