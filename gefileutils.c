//
// Created by Dan on 02.08.2022.
//

#include "gefileutils.h"










gint ge_mkdir_with_parents(gchar *pathname, gint mode, GError **error) {
    gint ret = g_mkdir_with_parents(pathname, mode);

    if (ret == -1) {
        ge_errno_set_error(error, errno);
    }

    return ret;
}










gboolean ge_file_set_contents_mkdir(gchar *filename, gchar *contents, gssize length, GError **error) {
    g_autofree gchar *pathname = g_path_get_dirname(filename);
    if (ge_mkdir_with_parents(pathname, 0700, error) == -1) return FALSE;
    return g_file_set_contents(filename, contents, length, error);
}
