//
// Created by dan on 15.03.2022.
//

#include "gefd.h"

void ge_fd_finalize(GObject *self);

G_DEFINE_TYPE(GEFD, ge_fd, GE_TYPE_OBJECT)

static void ge_fd_class_init(GEFDClass *class) {
    G_OBJECT_CLASS(class)->finalize = ge_fd_finalize;
}

static void ge_fd_init(GEFD *self) {

}

void ge_fd_finalize(GObject *self) {
    ge_fd_set_object(GE_FD(self), -1);

    G_OBJECT_CLASS(ge_fd_parent_class)->finalize(self);
}
