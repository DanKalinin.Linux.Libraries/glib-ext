//
// Created by root on 15.01.2020.
//

#include "gestrfuncs.h"

gchar *ge_strreplace(gchar *self, gchar *from, gchar *to) {
    g_auto(GStrv) strings = g_strsplit(self, from, 0);
    gchar *ret = g_strjoinv(to, strings);
    return ret;
}
