//
// Created by dan on 16.03.2022.
//

#include "getermios.h"

gint ge_tcgetattr(gint self, struct termios *termios_p, GError **error) {
    gint ret = tcgetattr(self, termios_p);

    if (ret == -1) {
        ge_errno_set_error(error, errno);
    }

    return ret;
}

gint ge_tcsetattr(gint self, gint optional_actions, struct termios *termios_p, GError **error) {
    gint ret = tcsetattr(self, optional_actions, termios_p);

    if (ret == -1) {
        ge_errno_set_error(error, errno);
    }

    return ret;
}

gint ge_cfsetspeed(struct termios *self, speed_t speed, GError **error) {
    gint ret = cfsetspeed(self, speed);

    if (ret == -1) {
        ge_errno_set_error(error, errno);
    }

    return ret;
}
